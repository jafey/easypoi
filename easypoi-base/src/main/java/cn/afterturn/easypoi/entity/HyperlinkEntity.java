package cn.afterturn.easypoi.entity;

/**
 * 超链接数据实体类
 * @author jafey
 *
 */
public class HyperlinkEntity {
	/**
	 * 单元格显示的内容
	 */
	private String cellValue;
	/**
	 * 超链接的地址，使用HYPERLINK表达式实现超链接
	 */
	private String address;

	public String getCellValue() {
		return cellValue;
	}

	public void setCellValue(String cellValue) {
		this.cellValue = cellValue;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
